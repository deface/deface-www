---
title: "Deface's advanced functionality"
date: 2019-01-13 00:00:00 -0500
categories: docs
anchor: advanced
type: process
tags: 🔐
---

<p>
  We believe the design outlined above increases the general level of privacy on social media platforms. There's however two shortcomings we would like to address in the future.
</p>

<ul>
  <li>
    🌍 Deface's proof of work mechanism has an environmental cost, since heavy computation drains a computer's battery more than normal. While we believe Facebook's untrustworthiness makes the company entirely responsible for this damage, we recognize the necessity to prevent redundant computation to the best of our ability.
  </li>
  <li>
    🐌 Not all computers are equal, and less powerful devices take longer to brute force encryption keys. This leaves individuals with slower devices with a choice between longer wait times and weaker protection. This is especially concerning because marginalized communities who benefit from this project the most are also those with least access to top of the line equipment.
  </li>
</ul>

<p>
  These issues can be mitigated by providing a mechanism that enables Deface clients to directly exchange encryption keys after two users have become Deface contacts.
</p>
  
<div class="media off l">
  <div class="label">
    In order to make Deface less computation-intensive for users, we will include a mechanism that lets people manage contacts and share content more efficiently with them.
  </div>
  <div class="image">
    <img data-original="{{ site.baseurl }}/images/deface/docs/key-exchange.png" width="1224" height="596" alt="Diagram of two users exchanging public keys"/>
  </div>
</div>

<p>
  From the user's perspective, this set of features boils down to making contact requests within the Facebook interface, and managing the resulting contact list.
</p>

<div class="media xl off">
  <div class="label">
    People are able to add Deface contacts directly from the Facebook interface. Doing so sends a notification to the recipient of the request, who can accept it. We also plan on allowing contacts <a href="https://ssd.eff.org/en/module/deep-dive-end-end-encryption-how-do-public-key-encryption-systems-work#4" target="_blank"> to verify each other's public key fingerprint</a>.
  </div>
  <div class="image" style="background-color: transparent;">
    <img data-original="{{ site.baseurl }}/images/deface/docs/add-contact.png" width="567" height="519" alt="A screen capture showing how Deface users can add contacts from the Facebook interface."/>
  </div>
  <div class="image" style="background-color: transparent;">
    <img data-original="{{ site.baseurl }}/images/deface/docs/contact-list.png" width="567" height="520" alt="A screen capture showing how Deface users process contact requests and manage their contact list."/>
  </div>
</div>

<p>
  After two users have become Deface contacts, their subsequent Deface posts are automatically sent to each other at the time they're posted on Facebook. Past messages can be requested at any time. This means friends are kept up to date with each other's content, which reduces the overall reliance on brute force decryption.
</p>

<div class="media xl off">
  <div class="label">
    Every time a user posts a Deface message, the encryption key is sent over to their Deface contacts. This allows them to skip the brute forcing process.
  </div>
  <div class="image">
    <img data-original="{{ site.baseurl }}/images/deface/docs/deface-diagram-3.png" width="1640" height="512" alt="A detailed diagram showing how Deface contacts access content while skipping the brute forcing of encryption keys."/>
  </div>
</div>

<p>
  In addition, we're considering adding an advanced security feature that limits content access to Deface contacts only. Opting for this would disable other users from accessing messages through proof of work. This option could be useful for communities with specific privacy needs.
</p>

<div class="media l off">
  <div class="label">
    This option allows users to broadcast secure messages to their entire Deface contact list, or a subset of it.
  </div>
  <div class="image" style="background-color: transparent;">
    <img data-original="{{ site.baseurl }}/images/deface/docs/direct-message.png" width="696" height="560" alt="Screen capture showing how Deface users can select contacts they intend to share content with."/>
  </div>
</div>

<p>
  Specification for this feature has yet to be written (<a href="https://deface.app/#contribute">help us</a>!). We are considering a centralized server that manages asynchronous communications between Deface clients. <a href="https://signal.org/docs/" target="_blank">Signal's Sesame algorithm</a> could be an option. In any case, the solution we select will need to meet security expectations and prove to be computationally cheaper than Deface's regular proof of work approach.
</p>