---
title: "Our approach to privacy"
date: 2019-01-15 00:00:00 -0500
categories: docs
anchor: privacy
type: process
tags: 🤖
---

<h5>Why not just quit?</h5>

<p>
  In the face of the blatant privacy abuses committed by Facebook, there's a growing consensus among the <a href="https://www.cnn.com/2019/04/15/tech/aoc-quit-facebook-just-like-me/index.html" target="_blank">general public</a> and tech experts that people should abandon the platform altogether. The open source community has been developing alternatives to mainstream social networks with a lot of success (e.g <a href="https://joinmastodon.org/" target="_blank">Mastodon</a>, <a href="https://www.scuttlebutt.nz/" target="_blank">Scuttlebutt</a>). So why are we creating a tool that gives people an excuse to remain on a platform whose financial interests are inherently opposed to protecting their communications?
</p>

<div class="media l off">
  <div class="label">
    We love Scuttlebutt and Mastodon too! Everyone should use them.
  </div>
  <div class="image">
    <img data-original="{{ site.baseurl }}/images/deface/docs/scuttlebutt-mastodon.png" width="1224" height="268" alt="Mastodon and Scuttlebutt logos"/>
  </div>
</div>

<p>
  While we agree people should consider alternatives, we believe expecting everyone to shut down their Facebook profile is an unrealistic approach that's rooted in privilege. There's a billion people who actively use Facebook, and each have their own reasons to do so. Some use Facebook because their livelihood depends on it, some others because that's where their community has settled. In a few words, people use Facebook because it works for them. Deface acknowledges the need for alternatives like Scuttlebutt, while focusing its efforts on providing legacy platforms with privacy tools that works for their users.
</p>

<p>
  Besides, Deface provides a model that will be replicable on other platforms. There's many other places online that could benefit from content obfuscation, and we hope to get there when the time comes.
</p>

<h5>Piggybacking on Facebook requires respecting its interaction model</h5>

<p>
  Many of the technical constraints that Deface deals with originate from our willingness to respect Facebook's interaction model. We set this goal for ourselves because we believe altering the platform's user experience would leave some people out. Every community who interacts on Facebook operates with its own codes, and making assumptions about how users should interact would automatically disrupt some people's ability to communicate.
</p>

<div class="media">
  <div class="image">
    <img data-original="{{ site.baseurl }}/images/deface/docs/marginalized.png" width="1224" height="268" alt="A diagram showing a user being marginalized because of lack of access to Deface content."/>
  </div>
</div>

<p>
  This would not only be an issue in terms of user growth, but could cause harm to people who did not ask for anything. Because Deface makes content unavailable for Facebook users who have not installed it, disrupting the platform's experience means pushing people away, splitting communities, and marginalizing individuals.
</p>

<h5>These constraints make public-key encryption unfit for our purpose</h5>

<p>
  Conventionally, software focused on privacy provides <a href="https://ssd.eff.org/en/module/deep-dive-end-end-encryption-how-do-public-key-encryption-systems-work" target="_blank">end-to-end encryption</a> features: secure communications that prevent third-parties from accessing data while it's transferred from one end device to another. In other words, when communicating via <a href="https://signal.org/" target="_blank">Signal</a> or <a href="https://wire.com/en/" target="_blank">Wire</a>, users can safely expect that only the sender and recipient will access the content of a message. These systems rely on public-key encryption. Deface's core functionality does not use public-key encryption, and does not provide the sort bulletproof protection expected from conventional privacy-focused applications. Why?
</p>

<p>
  An open conversation on a Facebook timeline is very different from a private conversation in a chatroom. By nature, public-key encryption facilitates communications between a sender and identified recipients. To be successful, this process requires users to maintain records of their contacts' trusted online identities. This is not viable on social media platforms where people organically deal with many tiers of relationships, from close friends to total strangers, and many levels in between.
</p>

<p>
  Admittedly, Deface could restrict its scope to users that have exchanged their public key information, but this would be redundant with the role already fulfilled by messaging apps like Signal or Wire... and a lot less secure.
</p>

<h5>Encryption vs obfuscation</h5>

<p>
  Traditional forms of encryption revealed too cumbersome to protect social media timelines, so we looked into other options. In 2018, we developed a prototype of Deface that decoupled Facebook content from the platform by posting content encryption keys to a <a href="https://www.youtube.com/watch?v=w9UObz8o8lY" target="_blank">distributed hash table</a>. Among others, that approach posed the question of user verification: such a distributed database would be open to everyone, so how could we prevent bad actors from quickly accessing all of the content? We initially opted to use reCaptcha, but came to the conclusion that relying on Google to merely slow down Facebook's access was not satisfactory.
</p>

<p>
  That's when we realized protecting people's public feeds would require a trade-off between user experience and security. We opted for obfuscation over encryption: a protection that's weaker for individuals, but could be more effective at scale. Because of its viral nature, Deface can spread across communities and enable people to protect each other by making personal data processing expensive and impractical.
</p>

<div class="media xl off">
  <div class="label">
    Obfuscation becomes most effective at scale.
  </div>
  <div class="image">
    <img data-original="{{ site.baseurl }}/images/deface/docs/impact-scale.png" width="1640" height="268" alt="Diagram showing algorithms breaking through an individual's messages, and struggling to monitor a larger group of people"/>
  </div>
</div>

<p>
  🚨 Opting for obfuscation over encryption means Deface, unlike conventional privacy tools, is not suitable for protecting the communications of dissidents and activists subjected to targeted surveillance. This distinction is critical to understand for Deface users, and in the future we will provide in-app information about the security trade-offs we make. 🚨
</p>
