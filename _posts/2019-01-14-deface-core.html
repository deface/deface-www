---
title: "Deface's core functionality"
date: 2019-01-14 00:00:00 -0500
categories: docs
anchor: core
type: process
tags: ⏳
---

<p>
  Now we've seen why Deface focuses on obfuscating people's content. This section covers how the app leverages the concept of proof of work in order to provide a layer of obfuscation on top of Facebook's interface.
</p>

<p>
  On one hand, an average social media user downloads a few hundred posts every day on their device. Each of those take a fraction of a second to load up. On the other hand, social media platforms process billions of messages. To do so efficiently, they need to access content very fast.
</p>

<div class="media xl off">
  <div class="label">
    
  </div>
  <div class="image">
    <img data-original="{{ site.baseurl }}/images/deface/docs/proof-of-work-1.png" width="1640" height="520" alt="Diagram showing how Facebook processes a lot more messages than simple users"/>
  </div>
</div>

<p>
  What if we put roadblocks in place, that slow down the access to each of those messages? Not slow enough that people would notice, but slow enough to render data processing impractical at scale. This is what Deface does: it slows down the internet a little bit, so only people can use it.
</p>

<div class="media xl off">
  <div class="label">
    Deface acts as a roadblock that slows down access to user content. Users and Facebook are both impacted, but the platform's need for fast access makes it especially vulnerable to this strategy.
  </div>
  <div class="image">
    <img data-original="{{ site.baseurl }}/images/deface/docs/proof-of-work-2.png" width="1640" height="520" alt="A roadblock slows done processing of every message. The user doesn't seem impacted, the robot bursts in flames."/>
  </div>
</div>

<p>
  To achieve this, Deface uses encryption of low entropy, a fancy term that means it can be broken easily by any computer, just like a weak password. Someone who wants to access a message simply needs to brute force the encryption key. This is what the app does when it detects a Deface post in someone's feed: it tries every character combination until it unlocks the encrypted content. For an average laptop, decrypting a message takes a couple of seconds.
</p>

<div class="media xl off">
  <div class="label">
    When a user posts a Deface message, the app encrypts it by generating a random encryption key. Later, when friends see that message in their feed, their Deface client unlocks it by trying every character combination until it matches the encryption key.
  </div>
  <div class="image">
    <img data-original="{{ site.baseurl }}/images/deface/docs/deface-diagram-2.png" width="1640" height="594" alt="Detailed diagram showing Deface's functionality: the app brute forces encryption keys to access messages."/>
  </div>
</div>

<p>
  Traditionally this would be considered extremely unsafe, since everyone can easily gain access to the encrypted information. But this is exactly what we're looking for: content available to everyone, that just takes an additional amount of computation to get to it. In other words, the encryption doesn't act as a protection, but as a <a href="https://en.wikipedia.org/wiki/Proof-of-work_system" target="_blank">proof of work</a> mechanism that confirms an actor has invested a given amount of resources to access a piece of content.
</p>

<div class="media l off">
  <div class="label">
    Deface encrypted messages contain a hash of the encryption key, the initialization vector, the length of the character set used to generate the key, and the cipher text.
  </div>
  <div class="image" style="background-color: rgb(231, 231, 236);">
    <img data-original="{{ site.baseurl }}/images/deface/docs/decryption.gif" width="640" height="360" style="max-width: 640px; margin: auto;" alt="An animated gif of Deface message decryption in action. Random characters turn into intelligible content."/>
  </div>
</div>

<p>
  For people, this is a minor inconvenience, as their friend's messages might take an extra second to load up. For Facebook, this is a major disruption, as the platform will need to engage a significant amount of computation to gain access to the content of all Deface users.
</p>

<p>
  In fact, if the cost of decrypting a message matches the revenue Facebook can derive from it, it becomes unprofitable to do so. In the future, we hope to modulate the complexity of encryption keys accordingly. For now, the interface lets people choose between incremental levels of security, based on their own privacy needs, the computing power available to their friends, or their own preference.
</p>

<div class="media off l">
  <div class="label">
    Users can select between several levels of encryption strength, which informs how difficult and costly it is to access their content.
  </div>
  <div class="image">
    <img data-original="{{ site.baseurl }}/images/deface/docs/encryption-strength.png" width="1224" height="592" alt="A diagram showing a user selecting between several levels of privacy, resulting in algorithms struggling incrementally."/>
  </div>
</div>

<p>
  This strategy, of course, relies on the premise that proof of work can in fact make personal data mining impractical at scale. We are in the process of evaluating this cost, and comparing it with the computation that Facebook already commits to processing personal content. This cost analysis needs to take into account how proof of work can be made more efficient when done at Facebook's scale. In parallel, we will need to understand the inconvenience experienced by Deface users, in terms of waiting time and battery life cost.
</p>

